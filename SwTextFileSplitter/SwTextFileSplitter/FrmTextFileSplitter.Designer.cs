﻿namespace SwTextFileSplitter
{
    partial class FrmTextFileSplitter
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTxtFileName = new System.Windows.Forms.TextBox();
            this.txtOutputDirectory = new System.Windows.Forms.TextBox();
            this.BtnBuscarTXT = new System.Windows.Forms.Button();
            this.BtnBusarDirectorio = new System.Windows.Forms.Button();
            this.BtnGenerar = new System.Windows.Forms.Button();
            this.BtnSalir = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.numUDMaxLinesPerFile = new System.Windows.Forms.NumericUpDown();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.lbldesc = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numUDMaxLinesPerFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Archivo de Texto";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Directorio de Salida";
            // 
            // txtTxtFileName
            // 
            this.txtTxtFileName.Location = new System.Drawing.Point(151, 19);
            this.txtTxtFileName.Name = "txtTxtFileName";
            this.txtTxtFileName.Size = new System.Drawing.Size(394, 20);
            this.txtTxtFileName.TabIndex = 2;
            // 
            // txtOutputDirectory
            // 
            this.txtOutputDirectory.Location = new System.Drawing.Point(151, 45);
            this.txtOutputDirectory.Name = "txtOutputDirectory";
            this.txtOutputDirectory.Size = new System.Drawing.Size(394, 20);
            this.txtOutputDirectory.TabIndex = 3;
            // 
            // BtnBuscarTXT
            // 
            this.BtnBuscarTXT.Location = new System.Drawing.Point(547, 19);
            this.BtnBuscarTXT.Name = "BtnBuscarTXT";
            this.BtnBuscarTXT.Size = new System.Drawing.Size(21, 20);
            this.BtnBuscarTXT.TabIndex = 4;
            this.BtnBuscarTXT.Text = "...";
            this.BtnBuscarTXT.UseVisualStyleBackColor = true;
            this.BtnBuscarTXT.Click += new System.EventHandler(this.BtnBuscarTXT_Click);
            // 
            // BtnBusarDirectorio
            // 
            this.BtnBusarDirectorio.Location = new System.Drawing.Point(547, 45);
            this.BtnBusarDirectorio.Name = "BtnBusarDirectorio";
            this.BtnBusarDirectorio.Size = new System.Drawing.Size(21, 20);
            this.BtnBusarDirectorio.TabIndex = 5;
            this.BtnBusarDirectorio.Text = "...";
            this.BtnBusarDirectorio.UseVisualStyleBackColor = true;
            this.BtnBusarDirectorio.Click += new System.EventHandler(this.BtnBusarDirectorio_Click);
            // 
            // BtnGenerar
            // 
            this.BtnGenerar.Location = new System.Drawing.Point(157, 124);
            this.BtnGenerar.Name = "BtnGenerar";
            this.BtnGenerar.Size = new System.Drawing.Size(129, 31);
            this.BtnGenerar.TabIndex = 6;
            this.BtnGenerar.Text = "&Generar";
            this.BtnGenerar.UseVisualStyleBackColor = true;
            this.BtnGenerar.Click += new System.EventHandler(this.BtnGenerar_Click);
            // 
            // BtnSalir
            // 
            this.BtnSalir.Location = new System.Drawing.Point(324, 124);
            this.BtnSalir.Name = "BtnSalir";
            this.BtnSalir.Size = new System.Drawing.Size(129, 31);
            this.BtnSalir.TabIndex = 7;
            this.BtnSalir.Text = "&Salir";
            this.BtnSalir.UseVisualStyleBackColor = true;
            this.BtnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Lineas Maximas P/Archivo";
            // 
            // numUDMaxLinesPerFile
            // 
            this.numUDMaxLinesPerFile.Location = new System.Drawing.Point(151, 71);
            this.numUDMaxLinesPerFile.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numUDMaxLinesPerFile.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUDMaxLinesPerFile.Name = "numUDMaxLinesPerFile";
            this.numUDMaxLinesPerFile.Size = new System.Drawing.Size(71, 20);
            this.numUDMaxLinesPerFile.TabIndex = 9;
            this.numUDMaxLinesPerFile.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // lbldesc
            // 
            this.lbldesc.Location = new System.Drawing.Point(12, 98);
            this.lbldesc.Name = "lbldesc";
            this.lbldesc.Size = new System.Drawing.Size(556, 15);
            this.lbldesc.TabIndex = 10;
            this.lbldesc.Text = "Generando....";
            this.lbldesc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmTextFileSplitter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 167);
            this.ControlBox = false;
            this.Controls.Add(this.lbldesc);
            this.Controls.Add(this.numUDMaxLinesPerFile);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BtnSalir);
            this.Controls.Add(this.BtnGenerar);
            this.Controls.Add(this.BtnBusarDirectorio);
            this.Controls.Add(this.BtnBuscarTXT);
            this.Controls.Add(this.txtOutputDirectory);
            this.Controls.Add(this.txtTxtFileName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTextFileSplitter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TextFile Splitter";
            ((System.ComponentModel.ISupportInitialize)(this.numUDMaxLinesPerFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTxtFileName;
        private System.Windows.Forms.TextBox txtOutputDirectory;
        private System.Windows.Forms.Button BtnBuscarTXT;
        private System.Windows.Forms.Button BtnBusarDirectorio;
        private System.Windows.Forms.Button BtnGenerar;
        private System.Windows.Forms.Button BtnSalir;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numUDMaxLinesPerFile;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label lbldesc;
    }
}

