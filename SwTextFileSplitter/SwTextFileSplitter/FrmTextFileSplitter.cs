﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwTextFileSplitter
{
    public partial class FrmTextFileSplitter : Form
    {
        public FrmTextFileSplitter()
        {
            InitializeComponent();
            lbldesc.Text = "";
            LoadConfig();
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            SaveConfig();
            Application.Exit();
        }

        private void BtnGenerar_Click(object sender, EventArgs e)
        {
            if (!ValidateControls())
            {
                return;
            }

            CreateFiles();
        }
        private bool ValidateControls()
        {
            errorProvider1.SetError(txtTxtFileName, "");
            if (!File.Exists(txtTxtFileName.Text))
            {
                errorProvider1.SetError(txtTxtFileName, "El Archivo no existe");
                return false;
            }

            errorProvider1.SetError(txtOutputDirectory, "");
            if (!Directory.Exists(txtOutputDirectory.Text ))
            {
                errorProvider1.SetError(txtOutputDirectory, "El Archivo no existe");
                return false;
            }

            return true;
        }

        private void CreateFiles()
        {
            int counter = 0;
            long maxLinesPerFile =(long) numUDMaxLinesPerFile.Value;
            int fileNumber = 1;

            string line;

            StreamWriter streamWriter = null;

            StreamReader file = new StreamReader(txtTxtFileName.Text);

            streamWriter = CreateStreamWriter(fileNumber);
            while ((line = file.ReadLine()) != null)
            {
                if (counter >= maxLinesPerFile)
                {
                    streamWriter.Flush();
                    counter = 0;
                    fileNumber++;
                    streamWriter = CreateStreamWriter(fileNumber);
                }

                streamWriter.WriteLine(line);
                counter++;
            }

            streamWriter.Flush();

            MessageBox.Show(string.Format("Se han creado {0} archivos de {1} lineas cada uno.", fileNumber, maxLinesPerFile), "Aviso al operador",  MessageBoxButtons.OK, MessageBoxIcon.Information);

            lbldesc.Text = "";
            lbldesc.Refresh();
        }

        private StreamWriter CreateStreamWriter(int fileNumber)
        {
            string outputFileName = Path.Combine(txtOutputDirectory.Text, Path.GetFileNameWithoutExtension(txtTxtFileName.Text) + "_" + string.Format("{0:0000}", fileNumber)  + Path.GetExtension(txtTxtFileName.Text));
            if (File.Exists(outputFileName))
                File.Delete(outputFileName);

            lbldesc.Text = string.Format("Generando el archivo {0}", outputFileName);
            lbldesc.Refresh();

            var fileStream = new FileStream(outputFileName, FileMode.CreateNew);
            return new StreamWriter(fileStream, Encoding.UTF8);
        }


        private void BtnBuscarTXT_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Text Files(*.txt;*.prn;*.dat;*.csv)|*.txt;*.prn;*.dat;*.csv";
            openFileDialog1.Multiselect = false;
            openFileDialog1.Title = "Archivos de Texto";
            openFileDialog1.ValidateNames = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtTxtFileName.Text = openFileDialog1.FileName;   
            }
        }

        private void BtnBusarDirectorio_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Directorio de Destino de los Archivos";
            folderBrowserDialog1.ShowNewFolderButton = true;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtOutputDirectory.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        void LoadConfig()
        {
            txtTxtFileName.Text = Properties.Settings.Default.FileName;
            txtOutputDirectory.Text = Properties.Settings.Default.OutputDirectory;
            numUDMaxLinesPerFile.Value = Properties.Settings.Default.MaxLinesPerFile;
        }

        void SaveConfig()
        {
            Properties.Settings.Default.FileName = txtTxtFileName.Text;
            Properties.Settings.Default.OutputDirectory = txtOutputDirectory.Text;
            Properties.Settings.Default.MaxLinesPerFile = (long) numUDMaxLinesPerFile.Value ;
            Properties.Settings.Default.Save();
        }

    }
}
